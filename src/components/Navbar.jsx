import React from 'react';
import { useNavigate } from 'react-router-dom'; // Import the useNavigate hook
import '../styles/navbar.css'; // Import the CSS file

export function Navbar() {
    const navigate = useNavigate(); // Initialize the navigate function

    return (
        <div className="navbar-container">
            <ul>
                {/* Use an arrow function to handle the click event */}
                <li onClick={() => navigate('/Startpage')}>
                    <a>sanbnb</a>
                </li>
                <li className='liright'>
                    <img onClick={() => navigate('/Datenschutz')} src="src\assets\iconUsers.png" alt="" />
                </li>
            </ul>
        </div>
    );
}
