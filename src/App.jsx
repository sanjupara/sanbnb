import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import { Navbar } from './components/Navbar'
import { Login } from './pages/Login'
import { Startpage } from './pages/Startpage'
import { LockerPage } from './pages/LockerPage'
import { BrowserRouter, Routes, Route } from "react-router-dom"
import { BookingPage } from './pages/BookingPage';
import { Datenschutz } from './pages/Datenschutz';

function App() {

  return (
    <>
      <BrowserRouter>
            <Routes>
              <Route path="/" element={<Login />} />
              <Route path="/Startpage" element={<Startpage/>} />
              <Route path="/Lockerpage" element={<LockerPage/>} />
              <Route path="/:LockerId" element={<BookingPage />} />
              <Route path="/Datenschutz" element={<Datenschutz/>} />
            </Routes>
        </BrowserRouter>
    </>
  )
}

export default App
