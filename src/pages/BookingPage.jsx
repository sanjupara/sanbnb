import React, { useState, useEffect, useRef } from 'react';
import { doc, getDoc } from "firebase/firestore";
import { Navbar } from './../components/Navbar';
import { useParams } from 'react-router-dom';
import { firestore } from "../firebaseconfig";
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import '../styles/bookingpage.css';


export function BookingPage() {
    const [locker, setLocker] = useState(null);
    const [loading, setLoading] = useState(true);
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(null);
    const [totalPrice, setTotalPrice] = useState(0);
    const { LockerId } = useParams();
    const paypalButtonRef = useRef(null);

    useEffect(() => {
        async function fetchLockerData() {
            try {
                if (LockerId) {
                    const lockerDocRef = doc(firestore, 'Lockers', LockerId);
                    const docSnap = await getDoc(lockerDocRef);
                    if (docSnap.exists()) {
                        setLocker(docSnap.data());
                    } else {
                        console.log('No such document for LockerId:', LockerId);
                    }
                }
            } catch (error) {
                console.error('Error fetching locker data:', error);
            } finally {
                setLoading(false);
            }
        }
        fetchLockerData();
    }, [LockerId]);


    useEffect(() => {
        if (!locker) return;
        const script = document.createElement("script");
        script.src = `https://www.paypal.com/sdk/js?client-id=${
            import.meta.env.VITE_PAYPAL_CLIENT_ID
        }&components=buttons`;
        script.onload = () => {
            window.paypal
                .Buttons({
                    createOrder: (data, actions) => {
                        return actions.order.create({
                            purchase_units: [
                                {
                                    description: "Locker Booking",
                                    amount: {
                                        currency_code: "USD",
                                        value: locker.Price,
                                    },
                                },
                            ],
                        });
                    },
                    onApprove: async (data, actions) => {
                        const order = await actions.order.capture();
                        console.log(order);
                    },
                    onError: (err) => {
                        console.error(err);
                    },
                })
                .render(paypalButtonRef.current);
        };
        document.body.appendChild(script);
    }, [locker]);


    const handleStartDateChange = (date) => {
        setStartDate(date);
    };

    const handleEndDateChange = (date) => {
        setEndDate(date);
        calculateTotalPrice(startDate, date);
    };

    const calculateTotalPrice = (startDate, endDate) => {
        if (startDate && endDate && locker) {
            const diffTime = Math.abs(endDate.getTime() - startDate.getTime());
            const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
            const totalPrice = diffDays * locker.Price;
            setTotalPrice(totalPrice);
        }
    };

    return (
        <>
            <Navbar />
            <div className="locker-info-container">
                {loading ? (
                    <div>Loading...</div>
                ) : locker ? (
                    <div className="locker-info">
                        <h1>{locker.Number}</h1>
                        <img src={locker.Image} alt="" className="locker-image" />
                        <div className="locker-details">
                            <h3>{locker.Price}Fr per day</h3>
                            <p>{locker.Description}</p>
                            <h4>Floor:{locker.Floor}</h4>
                        </div>
                    </div>
                ) : (
                    <div>No locker data found for ID: {LockerId}</div>
                )}
            </div>
            <div className="calendar-container">
                <Calendar
                    onChange={handleStartDateChange}
                    value={startDate}
                    selectRange={true}
                    minDate={new Date()}
                    maxDetail="month"
                    onClickDay={handleEndDateChange}
                />
            </div>
            <div className="total-price">
                {endDate && (
                    <p>Total Price: {totalPrice}Fr</p>
                )}
            </div>
            <div className="paypal-container">
                <div className="paypal" ref={paypalButtonRef} />
            </div>
            
        </>
    );
}
