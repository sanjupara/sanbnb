import { getFirestore, collection, getDocs, doc, getDoc } from "firebase/firestore";
import { Navbar } from './../components/Navbar';
import { useState, useEffect } from 'react';
import { firestore } from "../firebaseconfig";
import { useNavigate } from 'react-router-dom';

export function LockerPage() {
    const [lockers, setLockers] = useState([]);
    
    useEffect(() => {
        async function fetchData() {
            try {
                // Fetch a single locker
                const lockerDocRef = doc(firestore, 'Lockers', 'Locker1');
                const docSnap = await getDoc(lockerDocRef);
                if (docSnap.exists()) {
                    console.log("Single Locker:", docSnap.data());
                } else {
                    console.log('No such document!');
                }

                // Fetch all lockers
                const lockersColRef = collection(firestore, 'Lockers');
                const lockersSnapshot = await getDocs(lockersColRef);
                const lockersList = lockersSnapshot.docs.map(doc => doc.data());
                setLockers(lockersList);
                console.log("All Lockers:", lockersList);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }

        fetchData();
    }, []);

    return (
        <>
            <Navbar />
            <div>
              <input type="Floor" />
              <button>
                  Search
              </button>
            </div>
            <div>
                {lockers.map(locker => (
                    <div key={locker.id}>
                        <h1>{locker.Number}</h1>
                        <img src="{Locker.Image}" alt="" />
                        <h3>{locker.Price}</h3>
                        <p>{locker.Description}</p>
                        <h4>{locker.Floor}</h4>
                    </div>
                ))}
            </div>            
        </>
    );
}
