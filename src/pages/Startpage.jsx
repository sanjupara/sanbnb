import { Navbar } from './../components/Navbar';
import { useState, useEffect } from 'react';
import { collection, getDocs } from 'firebase/firestore'; // Importing necessary Firestore functions
import { firestore } from "../firebaseconfig";
import { useNavigate } from 'react-router-dom';
import video from '../assets/VideoGang.mp4';
import '../styles/startpage.css';

export function Startpage() {
    const [lockers, setLockers] = useState([]);
    const navigate = useNavigate();

    useEffect(() => {
        async function fetchData() {
            try {
                const lockersColRef = collection(firestore, 'Lockers');
                const lockersSnapshot = await getDocs(lockersColRef);
                const lockersList = lockersSnapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }));
                setLockers(lockersList);
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        }

        fetchData();
    }, []);

    const handleLockerClick = (lockerId) => {
        navigate(`/${lockerId}`); // Navigate to BookingPage with LockerId as route parameter
    };

    return (
        <>
            <Navbar />
            <br />
            <br />
            <br />
            <br />
            <div className="info-container">
                <div className="info-text">
                    <p>
                        Trete ein in die Welt der mühelosen Organisation und Sicherheit an unserer Schule. Wir freuen uns, dich auf unserer brandneuen Plattform begrüßen zu dürfen, die speziell für unsere Schüler entwickelt wurde, um ihre persönlichen Gegenstände sicher aufzubewahren.
                        Unsere Spinde sind nicht nur ein Ort, um deine Sachen zu verstauen, sondern auch eine Möglichkeit, deinen Tag problemlos zu gestalten. Von Büchern bis zu Sportausrüstung – wir haben Platz für alles.
                    </p>
                    <h4>
                        076 420 69 69
                    </h4>
                    <h4>
                        sanbnb@lockers.ch
                    </h4>
                </div>
                <div className="video-container">
                    <video width="240" height="240" loop controls src={video}></video>
                </div>
            </div>
            <br />
            <br />
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2695.718567925151!2d8.727989376386715!3d47.49539567117988!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x479a999e9fc4c32d%3A0xc5f240ca8689ae56!2sKantonsschule%20B%C3%BCelrain%20Winterthur%2C%20Wirtschaftsgymnasium%2C%20Handels-%20und%20Informatikmittelschule!5e0!3m2!1sde!2sch!4v1706197420508!5m2!1sde!2sch"
                width="1250"
                height="450"
                style={{ border: "0" }}
                allowFullScreen=""
                loading="lazy"
                referrerPolicy="no-referrer-when-downgrade"
            ></iframe>

            <div className="locker-container">
                {lockers.map(locker => (
                    <div className="locker-item" key={locker.id} onClick={() => handleLockerClick(locker.id)}>
                        <h1>{locker.Number}</h1>
                        <img src={locker.Image} alt="" />
                        <div className="locker-details">
                            <h3>{locker.Price}Fr</h3>
                            <p>{locker.Description}</p>
                            <h4>Floor:{locker.Floor}</h4>
                        </div>
                    </div>
                ))}
            </div>
        </>
    );
}
