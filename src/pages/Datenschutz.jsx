import React from "react";
import { Navbar } from "../components/Navbar";

export function Datenschutz() {
  return (
    <>
    <Navbar />
    <div>
      <h2 className="impressum-heading">SANBNB - Impressum</h2>
      <div className="impressum-content">
        <div className="impressum-section">
        </div>
      </div>
      <br />
        <h2>Datenschutzerklärung</h2>
        <br />
        <p>
        Durch die Nutzung unserer Website erklären Sie sich mit der nachfolgenden Datenschutzerklärung einverstanden. Sie können unsere Website grundsätzlich ohne Registrierung besuchen. Bei Ihrem Besuch werden bestimmte Daten wie aufgerufene Seiten oder Namen der abgerufenen Dateien, Datum und Uhrzeit zu statistischen Zwecken auf dem Server gespeichert. Diese Daten lassen keine direkte Rückschlüsse auf Ihre Person zu. Personenbezogene Daten wie Name, Adresse oder E-Mail-Adresse werden nur auf freiwilliger Basis erhoben. Ohne Ihre ausdrückliche Einwilligung werden keine Daten an Dritte weitergegeben.
        </p>
    </div>
    </>
  );
}
