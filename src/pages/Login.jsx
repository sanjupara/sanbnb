import React from 'react';
import { getAuth, signInWithPopup } from "firebase/auth";
import { googleProvider } from "../firebaseconfig";
import { ToastContainer, toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';
import '../styles/login.css';
import background from '../assets/LoginBackground.png';

export function Login() {
    const auth = getAuth();
    const navigate = useNavigate();

    const handleLogin = () => {
        const notify = () => toast.success("You're logged in!");
        const notify2 = () => toast.error("Something went wrong");

        signInWithPopup(auth, googleProvider)
            .then((result) => {
                notify();
                navigate('/Startpage');
            })
            .catch((error) => {
                if (error.code === "auth/popup-closed-by-user") return;
                notify2();
            });
    };

    return (
        <div className="login-container">
            <h1>Welcome to sanbnb</h1>
            <button onClick={handleLogin}>Login now with google</button>
            <ToastContainer />
        </div>
    );
}
