// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { GoogleAuthProvider } from "firebase/auth";
import { getFirestore } from "firebase/firestore";



// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyD3eSIdgt54mpcxkA5BN5octovod_bN7sk",
  authDomain: "sanbnb-43299.firebaseapp.com",
  projectId: "sanbnb-43299",
  storageBucket: "sanbnb-43299.appspot.com",
  messagingSenderId: "838545197881",
  appId: "1:838545197881:web:89f9a7eb4742f52f4976e8"
};




// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const googleProvider = new GoogleAuthProvider();
export const firestore = getFirestore();