FROM node:latest AS builder
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install
COPY . .
RUN yarn build

FROM node:latest
WORKDIR /app
COPY --from=builder /app/build ./build
RUN yarn global add firebase-tools
CMD ["firebase", "serve", "--only", "hosting"]
